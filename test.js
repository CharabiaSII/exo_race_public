// Vixen can fly 19 km/s for 7 seconds, but then must rest for 124 seconds.
// Rudolph can fly 3 km/s for 15 seconds, but then must rest for 28 seconds.
// Donner can fly 19 km/s for 9 seconds, but then must rest for 164 seconds.
// Blitzen can fly 19 km/s for 9 seconds, but then must rest for 158 seconds.
// Comet can fly 13 km/s for 7 seconds, but then must rest for 82 seconds.
// Cupid can fly 25 km/s for 6 seconds, but then must rest for 145 seconds.
// Dasher can fly 14 km/s for 3 seconds, but then must rest for 38 seconds.
// Dancer can fly 3 km/s for 16 seconds, but then must rest for 37 seconds.
// Prancer can fly 25 km/s for 6 seconds, but then must rest for 143 seconds.

class Reindeer {
    name;
    flyKm;
    flyTime;
    restTime;

    constructor(name, flyKm, flyTime, restTime) {
      this.name = name;
      this.flyKm = flyKm;
      this.flyTime = flyTime;
      this.restTime = restTime;
    }

    calculateDistanceByTime (totalTime){
        var totalKm = 0;
        var durationTime = 0;

        while(durationTime <= totalTime){
            if((totalTime - durationTime) >= this.flyTime){
                totalKm = totalKm + (this.flyKm*this.flyTime);
                durationTime = durationTime + this.flyTime + this.restTime;
                continue;
            }

            totalKm = totalKm + ((totalTime - durationTime)*this.flyKm);
            break;
        }
        // console.log(totalKm)
        return totalKm;
    }
}


const contestants = {
    Vixen : new Reindeer ('Vixen', 19, 7, 124),
    Rudolph : new Reindeer ('Rudolph', 3, 15, 28),
    Donner : new Reindeer ('Donner', 19, 9, 164),
    Blitzen : new Reindeer ('Blitzen', 19, 9, 158),
    Comet : new Reindeer ('Comet', 13, 7, 82),
    Cupid : new Reindeer ('Cupid', 25, 6, 145),
    Dasher : new Reindeer ('Dasher', 14, 3, 38),
    Dancer : new Reindeer ('Dancer', 3, 16, 37),
    Prancer : new Reindeer ('Prancer', 25, 6, 143)
};


function findWinner (totalTime, contestants){
    var winner;
    var totalKm = 0;
    Object.keys(contestants).forEach(key => {
        const reindeer = contestants[key]
        var temp = reindeer.calculateDistanceByTime(totalTime);
        if(temp > totalKm){
            winner = reindeer.name;
            totalKm = temp;
        }
    });
    console.log('The winner is ' + winner + ' who traveled a distance of ' + totalKm + ' km.')
}

findWinner(2503, contestants);



// const test = {
//     Comet : new Reindeer ('Comet', 14, 10, 127),
//     Dancer : new Reindeer ('Dancer', 16, 11, 162)
// };

// findWinner(1000, test);
// findWinner(2503, test);
